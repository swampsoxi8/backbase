//
//  AppController.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import SVProgressHUD

class AppController: UIViewController {


    //MARK: - Properties


    /// Singleton controller
    static let sharedController = AppController()

    /// Toggle bool to enable print outs for debugging
    var debugging = false

    /// eachibilty property
    var reachability = Reachability()




    //MARK: - Functions


    /// Parse JSON
    func fetchJSON(){

        SVProgressHUD.show(withStatus: "Fetching")
        SVProgressHUD.setDefaultMaskType(.gradient)

        JSONParser.fetchJSON { (complete) in

            SVProgressHUD.dismiss()

            NotificationCenter.default.removeObserver(self)
            
            switch complete{
            case .error(let error):

                /// Show user alert if needed
                self.showAlertWithTitle(title: error.rawValue)

            case .success(let JSON):

                for department in JSON{

                    JSONToCoreData.createCoreDataObjectFromJSON(jsonObject: department)
                }
            }
        }
    }

    /// Start network notifications
    func checkForNetworkConnection(){
        reachability = Reachability.forInternetConnection()
        reachability.startNotifier()
        updateInterfaceWithCurrent(networkStatus: reachability.currentReachabilityStatus())
    }


    func updateInterfaceWithCurrent(networkStatus: NetworkStatus) {
        switch networkStatus {
        case NotReachable:
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityStatusChanged(_:)), name: .reachabilityChanged, object: nil)
        case ReachableViaWiFi:
            fetchJSON()
        case ReachableViaWWAN:
            fetchJSON()
        default:
            return
        }
    }

    /// Update reachability state from `NSNotification`
    func reachabilityStatusChanged(_ sender: NSNotification) {
        guard let networkStatus = (sender.object as? Reachability)?.currentReachabilityStatus() else { return }
        updateInterfaceWithCurrent(networkStatus: networkStatus)
    }

    /// Bool to check if app has network access
    func appHasNetwork() -> Bool{
        switch reachability.currentReachabilityStatus() {
        case NotReachable:
            return false
        default:
            return true
        }
    }

    //MARK: Global alert

    func showAlertWithTitle(title:String?){
        DropDownAlert.addViewToSender(sender: UIViewController.topViewController()?.view, withTitle: title ?? "")
    }
}

/// Get top `UIViewController`
extension UIViewController {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if (base as? UIAlertController) != nil {
            return nil
        }

        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }

        return base
    }
}
