//
//  PersonTableViewCell.swift
//  Backbase
//
//  Created by Aaron Tredrea on 15/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {



    //MARK: - Outlets

    /// Image view for thumbnail
    @IBOutlet weak var thumbnailImageView: UIImageView!{didSet{
            thumbnailImageView.layer.cornerRadius = 8
            thumbnailImageView.layer.masksToBounds = true
        }
    }
    /// Label for name
    @IBOutlet weak var titleLabel: UILabel!
    /// ActivityIndicator whilst loading iage from the network
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!




    //MARK: - Functions

    /// Show ActivityIndicator
    func showSpinner(){
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }

     /// Hide ActivityIndicator
    func hideSpinner(){
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
}
