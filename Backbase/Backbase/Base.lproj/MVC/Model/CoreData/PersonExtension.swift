//
//  PersonExtension.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import CoreData

extension Person{
    class func createOrFetchPersonFromObject(personObject:PersonObject,inContext context:NSManagedObjectContext) -> Person?{

        /// Create request
        let request: NSFetchRequest<Person> = Person.fetchRequest()

        /// Use email as predicate but return if we do not have one
        guard let email = personObject.email else{
            return nil
        }

        request.predicate = NSPredicate(format: "email == %@", email)

        /// Create or fetch person
        do {
            let fetchedEmployees = try context.fetch(request)

            if fetchedEmployees.count == 0 {

                let person = createPersonFromPersonObject(personObject: personObject, inContext: context)
                person?.email = email
                return person

            }else{
                return fetchedEmployees.first
            }

        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
    }

    class func createPersonFromPersonObject(personObject:PersonObject, inContext context:NSManagedObjectContext) -> Person?{

        /// Create person from JSON object
        let person = Person(context: context)
        person.email = personObject.email
        person.name = personObject.name
        person.photo = personObject.photo
        person.role = personObject.role
        person.surname = personObject.surname

        return person
    }
}
