//
//  DetailViewController.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit
import MessageUI

class DetailViewController: UIViewController,MFMailComposeViewControllerDelegate {



    //MARK: - Properties


    /// Person property to populate info card
    var person:Person?
    /// Person Image to load button background image
    var personImage:UIImage?
    /// Store original frame to animate button frame from large to small
    var originalProfileThumbnailButtonFrame:CGRect?
    /// Bool to track image size state
    var imageIsFullScreen = false{didSet{
        UIView.animate(withDuration: 0.4, animations: {

            /// Toggle button image frame
            let newFrame = self.imageIsFullScreen ? self.detailView.bounds : self.originalProfileThumbnailButtonFrame!

            /// Toggle button image alpha
            self.backDropView.alpha = self.imageIsFullScreen ? 1 : 0

            /// Set button frmae
            self.profileThumbnailButton.frame = newFrame

            /// Toggle email button state as we animate image
            self.emailButton.isEnabled = !self.imageIsFullScreen
            self.emailButton.alpha = !self.imageIsFullScreen ? 1 : 0

        }, completion:nil)
        }
    }
    /// Bool to track if we have a valid person
    var personSelected = false{didSet{

        DispatchQueue.main.async {
            self.noPersonSelectedPromptLabel.isHidden = self.personSelected
            self.emailButton.isEnabled = self.personSelected
            self.emailButton.alpha = self.personSelected ? 1 : 0
        }
        }
    }



    //MARL: - Outlets


    /// Button to show profile image
    @IBOutlet weak var profileThumbnailButton: UIButton!{didSet{
        profileThumbnailButton.layer.cornerRadius = 12
        profileThumbnailButton.layer.masksToBounds = true
        }
    }
    /// Email button
    @IBOutlet weak var emailButton: UIButton!{
        didSet{
            emailButton.layer.cornerRadius = 20
            emailButton.layer.masksToBounds = true
        }
    }
    /// Background view to add depth whilst animating button image size
    @IBOutlet weak var backDropView: UIView!
    /// View to contain UI elements about person
    @IBOutlet weak var detailView: UIView!{
        didSet{
            detailView.layer.cornerRadius = 20
            detailView.layer.masksToBounds = true
        }
    }
    /// Person labels
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var noPersonSelectedPromptLabel: UILabel!


    //MARK: - Lifecysle


    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
        personSelected = person != nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /// Save original button frame
        originalProfileThumbnailButtonFrame = profileThumbnailButton.frame
    }


    //MARK: - Functions


    private func loadUI(){
        /// Load button images for states
        profileThumbnailButton.setBackgroundImage(personImage, for: .normal)
        profileThumbnailButton.setBackgroundImage(personImage, for: .selected)
        profileThumbnailButton.setBackgroundImage(personImage, for: .highlighted)

        /// Load labels
        nameLabel.text = "\(person?.name ?? "") \(person?.surname ?? "")"
        roleLabel.text = person?.role
        departmentLabel.text = person?.department
    }



    // MARK:- Actions


    /// Toggle image state for button
    @IBAction func imageButtonSelected(_ sender: Any) {
        imageIsFullScreen = !imageIsFullScreen
    }

    /// Send email button action
    @IBAction func emailSelected(_ sender: Any) {

        /// If use can send email then send email
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([person?.email ?? ""])
            present(mail, animated: true)
        } else {

            /// If use cant send email then copy address to clipboard
            UIPasteboard.general.string = person?.email ?? ""

            /// Inform user that email could not be drafted
            let alert = UIAlertController(title: "You can't currently send email", message: "email copied to clipboard", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))

            present(alert, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        controller.dismiss(animated: true, completion: nil)
    }
    
}
