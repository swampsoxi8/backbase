//
//  MasterTableViewController.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage
import Alamofire


class MasterTableViewController: UITableViewController {


    ///MARK: - Data

    struct Storyboard {
        static let detailSegue = "ShowDetailSegue"
    }


    //MARK:- Properties

    /// Fetch results controller to handle fetches
    lazy var fetchedResultsController: NSFetchedResultsController<Person> = {

        ///Create request
        let request: NSFetchRequest<Person> = Person.fetchRequest()

        /// Add sort descriptor
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(Person.department), ascending: true)]

        /// Create fetched results controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreDataManager.sharedDataManager.currentContext(), sectionNameKeyPath: #keyPath(Person.department), cacheName: nil)

        /// Set delegate to hadle updates
        fetchedResultsController.delegate = self

        return fetchedResultsController
    }()
    /// Property to store image to pass to next controller
    var selectedImage:UIImage?
    /// Property to store person to pass to next controller
    var person:Person?



    //MARK:- Lifecycle


    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        if fetchedResultsController.fetchedObjects?.count == 0{
            DropDownAlert.addViewToSender(sender: navigationController?.view, withTitle: "No results")
        }

        refreshControl?.addTarget(self, action: #selector(MasterTableViewController.handleRefresh), for: UIControlEvents.valueChanged)
    }




    //MARK:- Functions

    /// Fetch data
    private func fetchData(){
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("\(error.localizedDescription)")
        }
        refreshControl?.endRefreshing()
    }

    /// Handle pull to refesh
    func handleRefresh() {
        fetchData()
    }



    //MARK: - Navigation


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /// Pass properties to next controller
        if let detailVC = segue.destination as? DetailViewController{
            detailVC.personImage = selectedImage
            detailVC.person = person
        }
    }
}



//MARK:- UITableViewDataSource


extension MasterTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {

        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionInfo = fetchedResultsController.sections?[section] else {
            fatalError("Unexpected Section")
        }
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sectionInfo = fetchedResultsController.sections?[section] else {
            fatalError("Unexpected Section")
        }
        return sectionInfo.name
    }


    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 18)
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(PersonTableViewCell.self)", for: indexPath) as! PersonTableViewCell

        configure(cell , at: indexPath)

        return cell
    }

    /// Configure cell
    func configure(_ cell: PersonTableViewCell, at indexPath: IndexPath) {

        let person = fetchedResultsController.object(at: indexPath)

        cell.titleLabel.text = "\(person.name ?? "") \(person.surname ?? "")"

        /// Only proceed if we have a valid image
        guard let imageName = person.photo else{
            cell.hideSpinner()
            return
        }

        /// Create endpoint for image
        let url = "\(BaseURL.images+imageName)"

        /// Show spinner whilst fetching
        cell.showSpinner()


        if AppController.sharedController.appHasNetwork()
        {
            /// Fetch image and cache result
            Alamofire.request(url).responseImage { response in

                cell.hideSpinner()

                /// Load image to cell
                if let image = response.result.value {
                    cell.thumbnailImageView.image = image
                }
            }
        }else{
            DropDownAlert.addViewToSender(sender: UIViewController.topViewController()?.view, withTitle: "Please connect to internet")
        }
    }
}


//MARK:- UITableViewDelegate


extension MasterTableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let cell = tableView.cellForRow(at: indexPath) as! PersonTableViewCell

        /// Save values to pass to next controller
        selectedImage = cell.thumbnailImageView.image
        person = fetchedResultsController.object(at: indexPath)

        /// Start segue
        performSegue(withIdentifier: Storyboard.detailSegue, sender: nil)
    }
}


//MARK:- NSFetchedResultsControllerDelegate


extension MasterTableViewController: NSFetchedResultsControllerDelegate {


    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        refreshControl?.endRefreshing()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) {
                configure(cell as! PersonTableViewCell, at: indexPath)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }

            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            break;
        }
    }
}
