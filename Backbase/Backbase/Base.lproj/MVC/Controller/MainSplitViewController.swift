//
//  MainSplitViewController.swift
//  Backbase
//
//  Created by Aaron Tredrea on 15/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit

class MainSplitViewController: UISplitViewController,UISplitViewControllerDelegate {



    //MARK: - Lifecycle


    override func viewDidLoad() {
        self.delegate = self
        self.preferredDisplayMode = .allVisible
    }



    //MARK: - UISplitViewControllerDelegate

    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool{
        return true
    }
}
