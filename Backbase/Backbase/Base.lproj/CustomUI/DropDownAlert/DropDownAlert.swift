//
//  DropDownAlert.swift
//  Backbase
//
//  Created by Aaron Tredrea on 15/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import UIKit

class DropDownAlert: UIView {



    //MARK:- Outlets


    /// Blur view to surround label
    @IBOutlet weak var blurView: UIVisualEffectView!{
        didSet{
            blurView.layer.cornerRadius = 15
            blurView.layer.masksToBounds = true
        }
    }

    /// Title Label
    @IBOutlet weak var titleLabel: UILabel!



    //MARK: - Functions

    class func addViewToSender(sender:UIView?, withTitle title:String){

        /// Try to load XIB
        guard let dropDown = Bundle.main.loadNibNamed("\(DropDownAlert.self)", owner: self, options: nil)?.first as? DropDownAlert else{
            return
        }

        /// Only proceed if we have a valid view
        guard let sender = sender else {
            return
        }

        /// Set title
        dropDown.titleLabel.text = title

        /// Add view to parent
        sender.addSubview(dropDown)

        dropDown.translatesAutoresizingMaskIntoConstraints = false

        /// Add contraints to top and sides
        /// Make `topContsraint` a property so we can animate it
        let topConstraint = NSLayoutConstraint(item: dropDown, attribute: .top, relatedBy: .equal, toItem: sender, attribute: .top, multiplier: 1, constant: 0)
        topConstraint.constant = -100
        sender.addConstraint(topConstraint)
        sender.addConstraint(NSLayoutConstraint(item: dropDown, attribute: .trailingMargin, relatedBy: .equal, toItem: sender, attribute: .trailingMargin, multiplier: 1, constant: 0))
        sender.addConstraint(NSLayoutConstraint(item: dropDown, attribute: .leadingMargin, relatedBy: .equal, toItem: sender, attribute: .leadingMargin, multiplier: 1, constant: 0))

        /// Layout current constraints
        sender.layoutIfNeeded()

        /// Begin to animate alert
        UIView.animate(withDuration: AnimationSpeed.slow, delay: 0.4, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {

            /// Animate alert onto screen
            topConstraint.constant = 0
            sender.layoutIfNeeded()

        }, completion: { finished in
            UIView.animate(withDuration: AnimationSpeed.slow, delay: 2, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {

                /// Animate alert off of screen
                topConstraint.constant = -100
                sender.layoutIfNeeded()
                
            }, completion: nil)
        })
    }
}
