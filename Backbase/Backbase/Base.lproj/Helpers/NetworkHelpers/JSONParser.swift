//
//  JSONParser.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

enum APIError : String{
    case accessDenied
    case badRequest
    case notFound
    case couldNotParseJSON
    case unknown
    case userHasInternetButNoData = "You currently have no data in your package"
}

enum JSONParserCompletion {
    case error(error:APIError)
    case success(success: [String:Any])
}

class JSONParser:NSObject {

    class func fetchJSON(completion: @escaping (_ result: JSONParserCompletion) -> Void) {

        /// Fetch JSON from endpoint
        Alamofire.request(BaseURL.json).responseJSON(completionHandler: { (response) in

            if let JSON = response.result.value as? [String:Any] {

                /// Pass JSON back via completion block
                completion(.success(success: JSON))
            }else{

                if let error = response.result.error{
                    if error.localizedDescription == "The operation couldn’t be completed. Connection reset by peer"{
                        /// Handle error
                        completion(.error(error: .userHasInternetButNoData))
                    }
                }
            }
        })
    }
}



