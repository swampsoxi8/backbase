//
//  DepartmentObject.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//


import Decodable


struct DepartmentObject {
    let name: String?
}


extension DepartmentObject: Decodable {
    public static func decode(_ json: Any) throws -> DepartmentObject {

        return try DepartmentObject(
            name: json => "name")
    }
}
