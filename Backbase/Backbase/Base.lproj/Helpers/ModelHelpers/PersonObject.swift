//
//  PersonObject.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//


import Decodable


struct PersonObject {
    let email: String?
    let name: String?
    let photo: String?
    let role: String?
    let surname: String?
}


extension PersonObject: Decodable {
    public static func decode(_ json: Any) throws -> PersonObject {

        return try PersonObject(
            email: json => "email",
            name: json => "name",
            photo: json => "photo",
            role: json => "role",
            surname: json => "surname")
    }
}
