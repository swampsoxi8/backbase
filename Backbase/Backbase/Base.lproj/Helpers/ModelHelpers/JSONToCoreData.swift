//
//  JSONToCoreData.swift
//  Backbase
//
//  Created by Aaron Tredrea on 15/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import Foundation

class JSONToCoreData : NSObject{

    class func createCoreDataObjectFromJSON(jsonObject:(key:String,value:Any)){

        /// Get instance of `NSManagedObjectContext`
        let context = CoreDataManager.sharedDataManager.currentContext()

        context.perform {

            if let arrayOfDepartments = jsonObject.value as? [[String:Any]]{

                /// Loop through JSON and create `People` objects
                for person in arrayOfDepartments{

                    do {
                        /// Create object from JSON
                        let personObject = try PersonObject.decode(person)

                        /// Create or fetch core data object from JSON
                        guard let person = Person.createOrFetchPersonFromObject(personObject: personObject, inContext: context) else{
                            return
                        }
                        /// Set department value from JSON key
                        person.department = jsonObject.key
                        
                    } catch {
                        fatalError("Could not create object")
                    }
                }
            }
            CoreDataManager.sharedDataManager.saveContext()
        }
    }
}
