//
//  Constants.swift
//  Backbase
//
//  Created by Aaron Tredrea on 14/12/2016.
//  Copyright © 2016 swampsox. All rights reserved.
//

import Foundation
import UIKit


/// API Constants
struct BaseURL {
    static let json = "https://nielsmouthaan.nl/backbase/members.php"
    static let images = "https://nielsmouthaan.nl/backbase/photos/"
}


/// Numeric Constants
struct AnimationSpeed{
    static let normal:TimeInterval = 0.3
    static let medium:TimeInterval = 0.6
    static let slow:TimeInterval = 1.0

}

struct Size {
    static let deviceWidth = UIScreen.main.bounds.width
    static let deviceHeight = UIScreen.main.bounds.height
}
